//David Czeller
const csv = require('csv-parser', 'transactions.csv');
const fs = require('fs');

const shifts = [];
let advancedRows = [];

const transactions = [];
let advancedTransactions = [];

let restaurantOpen = 7;
let restaurantClose = 23;


const processWorkShifts = (row) => {
    const startTime = row.start_time.split(':');
    const endTime = row.end_time.split(':');
    const startTimeInMinutes = toMinutes(startTime);
    const endTimeInMinutes = toMinutes(endTime);
    const workMinutes = endTimeInMinutes - startTimeInMinutes;

    const breakArray = row.break_notes.split('-').map(x => x.trim());
    let breakStartInMinutes = getTime(breakArray[0]);
    if (breakStartInMinutes < startTimeInMinutes) breakStartInMinutes += 12 * 60;
    let breakEndInMinutes = getTime(breakArray[1]);
    if (breakEndInMinutes < startTimeInMinutes) breakEndInMinutes += 12 * 60;
    //finalnumbers in hour
    const breakLengthInMinutes = breakEndInMinutes - breakStartInMinutes;
    const payRate = parseInt(row.pay_rate);

    const advancedRow = {
        start1: startTimeInMinutes,
        end1: breakStartInMinutes,
        start2: breakEndInMinutes,
        end2: endTimeInMinutes,
        payRate
    }

    return advancedRow;

}

const getTime = (s) => {
    let toAdd = 0;
    const isPM = s.includes('PM');
    if (isPM) toAdd = 12 * 60;
    const value = toMinutes(s.replace('PM', '').replace('AM', '').trim().split('.'));
    return value + toAdd;
}

const toMinutes = (array) => {
    arr = array.map(a => parseInt(a));
    if (arr) {
        if (arr.length == 1) {
            return arr[0] * 60;
        } else if (arr.length == 2) {
            return arr[0] * 60 + arr[1];
        }
        throw new Error('Invalid time array format!');
    }
    return 0;
}

const processTransactions = (salesRows) => {
    const transactionAmount = salesRows.amount;
    const transactionTime = salesRows.time.split(':');
    const transactionTimeInMinutes = toMinutes(transactionTime);
    return {
        amount: transactionAmount,
        time: transactionTimeInMinutes
    };
}

const howMuchWork = (row, startTimeInMinutes, endTimeInMinutes) => {
    const work1 = [row.start1, row.end1];
    const work2 = [row.start2, row.end2];
    const checkingTime = [startTimeInMinutes, endTimeInMinutes];
    const a1 = intersection(work1, checkingTime);
    const a2 = intersection(work2, checkingTime);
    return intervalLength(a1) + intervalLength(a2);
}

const howMuchSales = (row, startTimeInMinutes, endTimeInMinutes) => {
    const amounts = row.filter(x => startTimeInMinutes < x.time && x.time < endTimeInMinutes).map(x => parseFloat(x.amount));
    const sum = amounts.reduce((a, b) => a + b, 0);
    return sum;
}

const intersection = (a, b) => {
    const result = [];

    result[0] = Math.max(a[0], b[0]);
    result[1] = Math.min(a[1], b[1]);

    if (result[0] > result[1]) {
        return null;
    }

    return result;
}

const intervalLength = (i) => {
    if (i) {
        return i[1] - i[0];
    }
    return 0;
}


fs.createReadStream('transactions.csv')
    .pipe(csv())
    .on('data', (salesRows) => {
        transactions.push(salesRows)
    })
    .on('end', () => {
        advancedTransactions = [...transactions.map(v => processTransactions(v))]
    });

fs.createReadStream('work_shifts.csv')
    .pipe(csv())
    .on('data', (row) => {
        shifts.push(row)
    })
    .on('end', () => {
        advancedRows = [...shifts.map(v => processWorkShifts(v))]

        const restaurantOpenInMinutes = Math.min(...advancedTransactions.map(x => x.time), ...advancedRows.map(x => x.start1));
        restaurantOpen = Math.floor(restaurantOpenInMinutes / 60);

        const restaurantCloseInMinutes = Math.max(...advancedTransactions.map(x => x.time), ...advancedRows.map(x => x.end2));
        restaurantClose = Math.floor(restaurantCloseInMinutes / 60);

        const table = [];

        const printRow = (r) => {
            const formattedPercenage = Number.isFinite(r.percentage) ? r.percentage.toFixed(2) + ' %' : " - ";
            console.log(`${r.i}:00`, r.sales.toFixed(2), r.labour.toFixed(2), formattedPercenage);
        }

        console.log();
        console.log('Hour:', 'Sales:', 'Labour:', '%');            
        for (let i = restaurantOpen; i < restaurantClose; i++) {
            let labour = 0;
            for (key in advancedRows) {
                let advancedRow = advancedRows[key];
                const workTime = howMuchWork(advancedRow, i * 60, (i + 1) * 60);
                labour += workTime * advancedRow.payRate / 60;
            }
            const sales = howMuchSales(advancedTransactions, i * 60, (i + 1) * 60);
            const percentage = labour / sales * 100;
            const result = {
                i,
                labour,
                sales,
                percentage
            }
            table.push(result);
            printRow(result);
        }
        const x = table
            // .filter(x => Number.isFinite(x.percentage))
            .sort((a, b) => a.percentage > b.percentage ? 1 : -1);
        const best = x[0];
        const worst = x[x.length - 1];
        console.log();
        console.log('Best hour of the day: ');
        printRow(best);
        console.log();        
        console.log('Worst hour of the day: ');
        printRow(worst);
    });

    //David Czeller